from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import landing_views
from .models import StatusModel
from .forms import StatusModelForm

# Create your tests here.
class TestStatusApp(TestCase):
    def setUp(self):
        self.client = Client()
        self.data_form = {
            'status': 'Coba Coba'
        }

    def test_ada_url_landing_page(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_tidak_ada_url_landing_page_slash_baru(self):
        response = self.client.get('/baru')
        self.assertEqual(response.status_code, 404)

    def test_fungsi_dipanggil_jika_url_landing_page_dinyalakan(self):
        response = resolve(reverse('status:landing'))
        self.assertEqual(response.func, landing_views)

    def test_bisa_membuat_models(self):
        StatusModel.objects.create(status='Coba Coba')
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertEqual(
            StatusModel.objects.get(id=1).status, 'Coba Coba'
        )

    def test_bisa_membuat_form(self):
        form = StatusModelForm(data=self.data_form)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertEqual(
            StatusModel.objects.get(id=1).status, 'Coba Coba'
        )

    def test_fungsi_landing_views_GET(self):
        response = self.client.get(reverse('status:landing'))
        content = response.content.decode('utf8')
        self.assertIn('<h1', content)
        self.assertIn('Status Alwan', content)

    def test_fungsi_landing_views_POST(self):
        response = self.client.post(reverse('status:landing'), {
            'status': 'Coba Coba'
        })

        self.assertEqual(response.status_code, 302)
        self.assertEqual(StatusModel.objects.get(id=1).status, 'Coba Coba')