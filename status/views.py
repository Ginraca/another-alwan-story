from django.shortcuts import render, redirect
from .models import StatusModel
from .forms import StatusModelForm
from django.http import HttpResponse

# Create your views here.
def landing_views(request):
    if request.method == 'POST':
        form = StatusModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status:landing')
    form = StatusModelForm()
    statuses = StatusModel.objects.all()
    response = {
        'form': form,
        'statuses': statuses
    }
    return render(request, 'base.html', response)