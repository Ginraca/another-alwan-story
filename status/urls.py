from django.contrib import admin
from django.urls import path
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.landing_views, name = 'landing')
]
